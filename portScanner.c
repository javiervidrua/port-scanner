#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <pthread.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <ctype.h>

int main(int argc, char * argv[]){

  printf("\t\t######  ####### ######  #######\n");
  printf("\t\t#     # #     # #     #    #\n");
  printf("\t\t#     # #     # #     #    #\n");
  printf("\t\t######  #     # ######     #\n");
  printf("\t\t#       #     # #   #      #\n");
  printf("\t\t#       #     # #    #     #\n");
  printf("\t\t#       ####### #     #    #\n");
  printf("\n");
  printf("\t #####   #####     #    #     # #     # ####### ######\n");
  printf("\t#     # #     #   # #   ##    # ##    # #       #     #\n");
  printf("\t#       #        #   #  # #   # # #   # #       #     #\n");
  printf("\t #####  #       #     # #  #  # #  #  # #####   ######\n");
  printf("\t      # #       ####### #   # # #   # # #       #   #\n");
  printf("\t#     # #     # #     # #    ## #    ## #       #    #\n");
  printf("\t #####   #####  #     # #     # #     # ####### #     #\n\n");

  // check arguments
  if (argc < 4){
    printf ("Usage: %s IPv4 First_Port Last_Port\n", argv[0]);
    exit(1);
  }

  // variables
  struct hostent *host;
  struct sockaddr_in sa;
  int err, i, sock, start, end;
  char hostname[64];
  char port;

  // arguments
  strcpy(hostname,argv[1]);
  if( (start=atoi(argv[2]))==-1 )
    exit(1);
  if( (end=atoi(argv[3]))==-1 )
    exit(1);

  // initialise the sockaddr_in structure
  memset ((char *)&sa, 0, sizeof(struct sockaddr_in));
  sa.sin_family = AF_INET;

  // direct ip address, use it
  if(isdigit(hostname[0])){
    printf("[+] - Doing inet_addr...");
    sa.sin_addr.s_addr=inet_addr(hostname);
    printf("Done\n");
  }

  // resolve hostname to ip address
  else if( (host=gethostbyname(hostname))!= 0 ){
    printf("[+] - Doing gethostbyname...");
    strncpy((char*)&sa.sin_addr , (char*)host->h_addr , sizeof sa.sin_addr);
    printf("Done\n");
  }
  else{
    herror(hostname);
    exit(2);
  }

  // start the port scan loop
  printf("[+] - Starting port scan: \n");
  for( i = start ; i <= end ; i++){

    // fill in the port number
    sa.sin_port = htons(i);

    // create a socket of type internet
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if(sock < 0){
      perror("[-] - Socket error");
      exit(3);
    }

    // connect using that socket and sockaddr structure
    err = connect(sock , (struct sockaddr*)&sa , sizeof sa);
    if( err < 0 ){
      printf("[+] - Port %d \t\tCLOSED\n", i);
    }
    else{
      printf("[+] - Port %d \t\tOPEN\n", i);
    }
    close(sock);
  }

  printf("\r\n");
  fflush(stdout);

  return 0;
}
